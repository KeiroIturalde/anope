/*
 *
 * (C) 2003-2014 Anope Team
 * Contact us at team@anope.org
 *
 * Please read COPYING and README for further details.
 *
 * Based on the original code of Epona by Lara.
 * Based on the original code of Services by Andy Church.
 */

#pragma once

#include "anope.h"
#include "base.h"
#include "bots.h"
#include "channels.h"
#include "commands.h"
#include "config.h"
#include "event.h"
#include "extensible.h"
#include "hashcomp.h"
#include "language.h"
#include "lists.h"
#include "logger.h"
#include "mail.h"
#include "messages.h"
#include "modes.h"
#include "modules.h"
#include "opertype.h"
#include "protocol.h"
#include "serialize.h"
#include "servers.h"
#include "service.h"
#include "services.h"
#include "socketengine.h"
#include "sockets.h"
#include "threadengine.h"
#include "timers.h"
#include "uplink.h"
#include "users.h"
#include "xline.h"

#include "modules/chanserv.h"
#include "modules/nickserv.h"
#include "modules/botserv.h"
#include "modules/memoserv.h"
