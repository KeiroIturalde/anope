/*
 * (C) 2003-2014 Anope Team
 * Contact us at team@anope.org
 *
 * Please read COPYING and README for further details.
 */

namespace WebCPanel
{

namespace ChanServ
{

extern void BuildChanList(::NickServ::Nick *, TemplateFileServer::Replacements &);

}

}

