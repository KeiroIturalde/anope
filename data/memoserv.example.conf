/*
 * Example configuration file for MemoServ.
 */

/*
 * First, create the service.
 */
service
{
	/*
	 * The name of the MemoServ client.
	 * If you change this value, you probably want to change the client directive in the configuration for the memoserv module too.
	 */
	nick = "MemoServ"

	/*
	 * The username of the MemoServ client.
	 */
	user = "services"

	/*
	 * The hostname of the MemoServ client.
	 */
	host = "services.host"

	/*
	 * The realname of the MemoServ client.
	 */
	gecos = "Memo Service"

	/*
	 * The modes this client should use.
	 * Do not modify this unless you know what you are doing.
	 *
	 * These modes are very IRCd specific. If left commented, sane defaults
	 * are used based on what protocol module you have loaded.
	 *
	 * Note that setting this option incorrectly could potentially BREAK some, if
	 * not all, usefulness of the client. We will not support you if this client is
	 * unable to do certain things if this option is enabled.
	 */
	#modes = "+o"
}

/*
 * Core MemoServ module.
 *
 * Provides essential functionality for MemoServ.
 */
module
{
	name = "memoserv/main"
	/*
	 * The name of the client that should be MemoServ. Clients are configured
	 * with the service blocks.
	 */
	client = "MemoServ"

	/*
	 * The maximum number of memos a user is allowed to keep by default. Normal users may set the
	 * limit anywhere between 0 and this value. Services Admins can change it to any value or
	 * disable it.
	 *
	 * This directive is optional, but recommended. If not set, the limit is disabled
	 * by default, and normal users can set any limit they want.
	 */
	maxmemos = 20

	/*
	 * The delay between consecutive uses of the MemoServ SEND command. This can help prevent spam
	 * as well as denial-of-service attacks from sending large numbers of memos and filling up disk
	 * space (and memory). The default 3-second wait means a maximum average of 150 bytes of memo
	 * per second per user under the current IRC protocol.
	 *
	 * This directive is optional, but recommended.
	 */
	senddelay = 3s
}

/*
 * Core MemoServ commands.
 *
 * In Anope modules can provide (multiple) commands, each of which has a unique command name. Once these modules
 * are loaded you can then configure the commands to be added to any client you like with any name you like.
 *
 * Additionally, you may provide a permission name that must be in the opertype of users executing the command.
 *
 * Sane defaults are provided below that do not need to be edited unless you wish to change the default behavior.
 */

/* Give it a help command. */
command { service = "MemoServ"; name = "HELP"; command = "generic/help"; }

/*
 * memoserv/cancel
 *
 * Provides the command memoserv/cancel.
 *
 * Used to cancel memos already sent but not yet read.
 */
module { name = "memoserv/cancel" }
command { service = "MemoServ"; name = "CANCEL"; command = "memoserv/cancel"; }

/*
 * memoserv/check
 *
 * Provides the command memoserv/check.
 *
 * Used to check if a sent memo has been read.
 */
module { name = "memoserv/check" }
command { service = "MemoServ"; name = "CHECK"; command = "memoserv/check"; }

/*
 * memoserv/del
 *
 * Provides the command memoserv/del.
 *
 * Used to delete your memos.
 */
module { name = "memoserv/del" }
command { service = "MemoServ"; name = "DEL"; command = "memoserv/del"; }

/*
 * memoserv/ignore
 *
 * Provides the command memoserv/ignore.
 *
 * Used to ignore memos from specific users.
 */
module
{
	name = "memoserv/ignore"

	/*
	 * The maximum number of entries that may be on a memo ignore list.
	 *
	 * This directive is optional.
	 */
	max = 32
}
command { service = "MemoServ"; name = "IGNORE"; command = "memoserv/ignore"; }

/*
 * memoserv/info
 *
 * Provides the command memoserv/info.
 *
 * Used to show memo related information about an account or a channel.
 */
module { name = "memoserv/info" }
command { service = "MemoServ"; name = "INFO"; command = "memoserv/info"; }

/*
 * memoserv/list
 *
 * Provides the command memoserv/list.
 *
 * Used to list your current memos.
 */
module { name = "memoserv/list" }
command { service = "MemoServ"; name = "LIST"; command = "memoserv/list"; }

/*
 * memoserv/read
 *
 * Provides the command memoserv/read.
 *
 * Used to read your memos.
 */
module { name = "memoserv/read" }
command { service = "MemoServ"; name = "READ"; command = "memoserv/read"; }

/*
 * memoserv/rsend
 *
 * Provides the command memoserv/rsend.
 *
 * Used to send a memo requiring a receipt be sent back once it is read.
 *
 * Requires configuring memoserv:memoreceipt.
 */
#module
{
	name = "memoserv/rsend"

	/*
	 * Only allow Services Operators to use memoserv/rsend.
	 *
	 * This directive is optional.
	 */
	operonly = false
}
#command { service = "MemoServ"; name = "RSEND"; command = "memoserv/rsend"; }

/*
 * memoserv/send
 *
 * Provides the command memoserv/send.
 *
 * Used to send memos.
 */
module { name = "memoserv/send" }
command { service = "MemoServ"; name = "SEND"; command = "memoserv/send"; }

/*
 * memoserv/sendall
 *
 * Provides the command memoserv/sendall.
 *
 * Used to send a mass memo to every registered user.
 */
module { name = "memoserv/sendall" }
command { service = "MemoServ"; name = "SENDALL"; command = "memoserv/sendall"; permission = "memoserv/sendall"; }

/*
 * memoserv/set
 *
 * Provides the command memoserv/set.
 *
 * Used to set settings such as how you are notified of new memos, and your memo limit.
 */
module { name = "memoserv/set" }
command { service = "MemoServ"; name = "SET"; command = "memoserv/set"; }

/*
 * memoserv/staff
 *
 * Provides the command memoserv/staff.
 *
 * Used to send a memo to all registered staff members.
 */
module { name = "memoserv/staff" }
command { service = "MemoServ"; name = "STAFF"; command = "memoserv/staff"; permission = "memoserv/staff"; }
