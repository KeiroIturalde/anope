/*
 * Example configuration file for HostServ.
 */

/*
 * First, create the service.
 */
service
{
	/*
	 * The name of the HostServ client.
	 * If you change this value, you probably want to change the client directive in the configuration for the hostserv module too.
	 */
	nick = "HostServ"

	/*
	 * The username of the HostServ client.
	 */
	user = "services"

	/*
	 * The hostname of the HostServ client.
	 */
	host = "services.host"

	/*
	 * The realname of the HostServ client.
	 */
	gecos = "vHost Service"

	/*
	 * The modes this client should use.
	 * Do not modify this unless you know what you are doing.
	 *
	 * These modes are very IRCd specific. If left commented, sane defaults
	 * are used based on what protocol module you have loaded.
	 *
	 * Note that setting this option incorrectly could potentially BREAK some, if
	 * not all, usefulness of the client. We will not support you if this client is
	 * unable to do certain things if this option is enabled.
	 */
	#modes = "+o"
}

/*
 * Core HostServ module.
 *
 * Provides essential functionality for HostServ.
 */
module
{
	name = "hostserv/main"

	/*
	 * The name of the client that should be HostServ.
	 */
	client = "HostServ"

	/*
	 * If enabled, vhosts are activated on users immediately when they are set.
	 */
	activate_on_set = false
}

/*
 * Core HostServ commands.
 *
 * In Anope modules can provide (multiple) commands, each of which has a unique command name. Once these modules
 * are loaded you can then configure the commands to be added to any client you like with any name you like.
 *
 * Additionally, you may provide a permission name that must be in the opertype of users executing the command.
 *
 * Sane defaults are provided below that do not need to be edited unless you wish to change the default behavior.
 */

/* Give it a help command. */
command { service = "HostServ"; name = "HELP"; command = "generic/help"; }

/*
 * hostserv/del
 *
 * Provides the commands hostserv/del and hostserv/delall.
 *
 * Used for removing users' vHosts.
 */
module { name = "hostserv/del" }
command { service = "HostServ"; name = "DEL"; command = "hostserv/del"; permission = "hostserv/del"; }
command { service = "HostServ"; name = "DELALL"; command = "hostserv/delall"; permission = "hostserv/del"; }

/*
 * hostserv/group
 *
 * Provides the command hostserv/group.
 *
 * Used for grouping one vHost to many nicks.
 */
module
{
	name = "hostserv/group"

	/*
	 * Upon nickserv/group, this option syncs the nick's main vHost to the grouped nick.
	 */
	syncongroup = false

	/*
	 * This makes vhosts act as if they are per account.
	 */
	synconset = false
}
command { service = "HostServ"; name = "GROUP"; command = "hostserv/group"; }

/*
 * hostserv/list
 *
 * Provides the command hostserv/list.
 *
 * Used for listing actively set vHosts.
 */
module { name = "hostserv/list" }
command { service = "HostServ"; name = "LIST"; command = "hostserv/list"; permission = "hostserv/list"; }

/*
 * hostserv/off
 *
 * Provides the command hostserv/off.
 *
 * Used for turning off your vHost.
 */
module { name = "hostserv/off" }
command { service = "HostServ"; name = "OFF"; command = "hostserv/off"; }

/*
 * hostserv/on
 *
 * Provides the command hostserv/on.
 *
 * Used for turning on your vHost.
 */
module { name = "hostserv/on" }
command { service = "HostServ"; name = "ON"; command = "hostserv/on"; }

/*
 * hostserv/request
 *
 * Provides the commands hostserv/request, hostserv/activate, hostserv/reject, and hostserv/waiting.
 *
 * Used to manage vHosts requested by users.
 */
module
{
	name = "hostserv/request"

	/*
	 * If set, Services will send a memo to the user requesting a vHost when it's been
	 * approved or rejected.
	 */
	#memouser = yes

	/*
	 * If set, Services will send a memo to all Services staff when a new vHost is requested.
	 */
	#memooper = yes
}
command { service = "HostServ"; name = "REQUEST"; command = "hostserv/request"; }
command { service = "HostServ"; name = "ACTIVATE"; command = "hostserv/activate"; permission = "hostserv/set"; }
command { service = "HostServ"; name = "REJECT"; command = "hostserv/reject"; permission = "hostserv/set"; }
command { service = "HostServ"; name = "WAITING"; command = "hostserv/waiting"; permission = "hostserv/set"; }

/*
 * hostserv/set
 *
 * Provides the commands hostserv/set and hostserv/setall.
 *
 * Used for setting users' vHosts.
 */
module { name = "hostserv/set" }
command { service = "HostServ"; name = "SET"; command = "hostserv/set"; permission = "hostserv/set"; }
command { service = "HostServ"; name = "SETALL"; command = "hostserv/setall"; permission = "hostserv/set"; }
